import Axios, { AxiosResponse } from "axios";
import React, { useState } from "react";
import styled from "styled-components";
import PokemonApi, { Pokemon, PokemonAll } from "./Api/Pokemons/Pokemons";
import "./App.css";
import Button from "./Components/Button/Button";
import PokeBallLoader from "./Components/PokeBallLoader/PokeBallLoader";
import Pokedex from "./Components/Pokedex/Pokedex";
import PokemonHeader from "./Components/PokemonHeader/PokemonHeader";
import StartButton from "./Components/StartButton/StartButton";

const BrowseButtonsContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 5em;
  @media (max-width: 300px) {
    display: inline-grid;
    margin-top: 1em;
  }
`;

const StartButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 10%;
  @media (max-width: 300px) {
    margin-top: 30%;
  }
`;

const App = () => {
  const [pokemons, setPokemons] = useState<Array<Pokemon>>([]);
  const [nextUrl, setNextUrl] = useState<string>();
  const [previousUrl, setPreviousUrl] = useState<string>();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const initialUrl = "https://pokeapi.co/api/v2/pokemon";

  const fetchMultiplePokemons = async (
    result: [
      {
        name: string;
        url: string;
      }
    ]
  ) => {
    return Promise.all(result.map((item) => PokemonApi.oneByName(item.name)));
  };

  const getPokemons = (url: string) => {
    Axios.get(url)
      .then(({ data }: AxiosResponse<PokemonAll>) => {
        setNextUrl(data.next);
        setPreviousUrl(data.previous);
        return data;
      })
      .then((data) => {
        return fetchMultiplePokemons(data.results);
      })
      .then((pokemons) => setPokemons(pokemons))
      .finally(() => setIsLoading(false));
  };
  const HandleBrowseButtonClick = (button: string) => {
    if (button === "previous" && previousUrl) {
      setIsLoading(true);
      getPokemons(previousUrl);
    } else if (button === "next" && nextUrl) {
      setIsLoading(true);
      getPokemons(nextUrl);
    } else if (button === "initial") {
      setIsLoading(true);
      getPokemons(initialUrl);
    }
  };

  return (
    <>
      <PokemonHeader />

      {pokemons.length < 1 ? (
        <StartButtonContainer>
          <StartButton
            onClick={() => HandleBrowseButtonClick("initial")}
          ></StartButton>
        </StartButtonContainer>
      ) : (
        <>
          <BrowseButtonsContainer>
            {previousUrl && (
              <Button
                buttonText={"Previous"}
                onClick={() => HandleBrowseButtonClick("previous")}
              ></Button>
            )}
            {nextUrl && (
              <Button
                buttonText={"Next"}
                onClick={() => HandleBrowseButtonClick("next")}
              ></Button>
            )}
          </BrowseButtonsContainer>
          {isLoading ? (
            <PokeBallLoader />
          ) : (
            <>
              <Pokedex pokemons={pokemons}></Pokedex>
            </>
          )}
        </>
      )}
    </>
  );
};

export default App;
