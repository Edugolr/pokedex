import React from "react";
import { fireEvent, render } from "@testing-library/react";
import StartButton from "./StartButton";

const onCLick = jest.fn();

test("renders startbutton", () => {
  const { queryByText, getByText } = render(<StartButton onClick={onCLick} />);
  expect(queryByText(/Pokemon/i)).toBeTruthy();
  const button = getByText(/Pokemon/i);

  fireEvent.mouseOver(button);
  expect(queryByText(/go/i)).toBeTruthy();

  fireEvent.click(button);
  expect(onCLick).toHaveBeenCalledTimes(1);
});

test("should match snapshot", () => {
  const { container } = render(<StartButton onClick={onCLick} />);
  expect(container).toMatchSnapshot();
});
