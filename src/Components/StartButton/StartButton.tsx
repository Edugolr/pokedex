import React, { FunctionComponent, useState } from "react";
import styled from "styled-components";
import { ButtonTextStyled } from "../Button/Button";

interface StartButtonProps {
  onClick: (event: React.MouseEvent<HTMLElement>) => void;
}

const StartButtonStyled = styled.button`
  background: red;
  min-width: max-content;
  cursor: pointer;
  width: 10%;
  &:hover {
    background: black;
  }
`;

const StartButton: FunctionComponent<StartButtonProps> = (props) => {
  const { onClick } = props;
  const initialButtonText = "Pokemon";
  const [buttonText, setButtonText] = useState("Pokemon");
  return (
    <StartButtonStyled
      onMouseOver={() => setButtonText("Go")}
      onMouseLeave={() => setButtonText(initialButtonText)}
      onClick={onClick}
    >
      <ButtonTextStyled>{buttonText}</ButtonTextStyled>
    </StartButtonStyled>
  );
};

export default StartButton;
