import React, { FunctionComponent } from "react";
import styled from "styled-components";

const ButtonStyled = styled.button`
  background: red;
  min-width: max-content;
  cursor: pointer;
  width: 10%;
  &:hover {
    background: black;
  }
  margin-left: 0.5em;
  margin-right: 2em;
`;
export const ButtonTextStyled = styled.p`
  font-family: "PokemonSolid";
  font-size: larger;
  color: white;
  margin: 0.5em auto;
`;

interface ButtonProps {
  onClick: (event: React.MouseEvent<HTMLElement>) => void;
  buttonText: string;
}

const Button: FunctionComponent<ButtonProps> = (props) => {
  const { onClick, buttonText } = props;
  return (
    <ButtonStyled onClick={onClick}>
      <ButtonTextStyled>{buttonText}</ButtonTextStyled>
    </ButtonStyled>
  );
};

export default Button;
