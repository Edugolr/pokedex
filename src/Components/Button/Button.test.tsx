import React from "react";
import { fireEvent, render } from "@testing-library/react";
import Button from "./Button";

const onCLick = jest.fn();

test("renders button", () => {
  const { getByText } = render(
    <Button onClick={onCLick} buttonText="buttonText" />
  );
  const button = getByText(/ButtonText/i);
  fireEvent.click(button);
  expect(onCLick).toHaveBeenCalledTimes(1);
});

test("should match snapshot", () => {
  const { container } = render(
    <Button onClick={onCLick} buttonText="buttonText" />
  );
  expect(container).toMatchSnapshot();
});
