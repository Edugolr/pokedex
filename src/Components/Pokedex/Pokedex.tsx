import React, { FunctionComponent } from "react";
import styled from "styled-components";
import { Pokemon } from "../../Api/Pokemons/Pokemons";
import PokemonCardSmall from "../PokemonCard/PokemonCard";

interface PokedexProps {
  pokemons: Pokemon[];
}
const PokemonCardsContainer = styled.div`
  width: 80%;
  margin: auto;
  display: grid;
  grid-template-columns: auto auto auto auto;
  padding-top: 2%;
  @media (max-width: 768px) {
    grid-template-columns: auto auto auto;
  }
  @media (max-width: 468px) {
    grid-template-columns: auto auto;
  }
  @media (max-width: 300px) {
    grid-template-columns: auto;
  }
`;

const Pokedex: FunctionComponent<PokedexProps> = (props) => {
  const { pokemons } = props;
  return (
    <PokemonCardsContainer>
      {pokemons.map((pokemon) => {
        return <PokemonCardSmall pokemon={pokemon}></PokemonCardSmall>;
      })}
    </PokemonCardsContainer>
  );
};

export default Pokedex;
