import React, { FunctionComponent } from "react";
import styled from "styled-components";
import { RotateAnimation } from "../../Utils/Animations/Rotate";

const PokeBallWrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const PokeBall = styled.div`
  width: 60px;
  height: 60px;
  background-color: #eee7e7;
  border-radius: 50%;
  overflow: hidden;
  border: 1px solid;
  animation: ${RotateAnimation} 2s linear infinite;
  &:after {
    content: "";
    position: absolute;
    width: 60px;
    height: 30px;
    background-color: red;
    border-bottom: 4px solid;
  }
  &:before {
    content: "";
    position: absolute;
    background-color: #fff;
    width: 10px;
    height: 10px;
    border: 4px solid;
    border-radius: 50%;
    bottom: 19px;
    right: 20px;
    z-index: 1;
  }
`;

const LoadingText = styled.p`
  font-family: "PokemonSolid";
  font-size: larger;
`;

const PokeBallLoader: FunctionComponent = () => {
  return (
    <>
      <PokeBallWrapper>
        <PokeBall />
        <LoadingText>Loading...</LoadingText>
      </PokeBallWrapper>
    </>
  );
};

export default PokeBallLoader;
