import React from "react";
import { render } from "@testing-library/react";
import PokeBallLoader from "./PokeBallLoader";

test("renders pokeball loader", () => {
  const { getByText } = render(<PokeBallLoader />);
  const LoadingText = getByText(/Loading../i);
  expect(LoadingText).toBeInTheDocument();
});

test("should match snapshot", () => {
  const { container } = render(<PokeBallLoader />);
  expect(container).toMatchSnapshot();
});
