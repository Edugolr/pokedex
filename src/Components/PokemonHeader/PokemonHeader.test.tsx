import React from "react";
import { render } from "@testing-library/react";
import PokemonHeader from "./PokemonHeader";

test("renders header", () => {
  const { getByText } = render(<PokemonHeader />);
  const PokemonText = getByText(/Pokemon/i);
  expect(PokemonText).toBeInTheDocument();
});

test("should match snapshot", () => {
  const { container } = render(<PokemonHeader />);
  expect(container).toMatchSnapshot();
});
