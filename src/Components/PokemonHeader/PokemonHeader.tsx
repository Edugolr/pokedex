import React, { FunctionComponent } from "react";
import styled from "styled-components";

const PokemonHeaderText = styled.header`
  font-family: "PokemonSolid";
  font-size: xxx-large;
  color: white;
  text-align: center;
  padding-top: 0.2em;
  background: linear-gradient(red 76%, black 24%);
`;
const PokemontHeaderWrapper = styled.div`
  height: 80px;
`;

const PokemonHeader: FunctionComponent = () => {
  return (
    <PokemontHeaderWrapper>
      <PokemonHeaderText>Pokemon</PokemonHeaderText>
    </PokemontHeaderWrapper>
  );
};

export default PokemonHeader;
