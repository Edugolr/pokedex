import React, { FunctionComponent, useState } from "react";
import styled from "styled-components";
import { Pokemon } from "../../Api/Pokemons/Pokemons";
import { BounceAnimation } from "../../Utils/Animations/Bouncing";
import { PokemonTypeToColor } from "../../Utils/PokemonTypeToColor";

interface PokemonCardProps {
  pokemon: Pokemon;
}

const Name = styled.p`
  margin: auto;
  text-align: center;
  font-family: "Roboto";
  text-transform: capitalize;
`;
const Type = styled.p`
  font-family: "Roboto";
  text-transform: capitalize;
`;

const Number = styled.p`
  margin: auto;
  text-align: center;
  float: right;
`;

const Sprite = styled.img`
  display: block;
  margin-left: auto;
  margin-right: auto;
  &:hover {
    animation: ${BounceAnimation} 1s ease infinite;
  }
`;

const Chip = styled.div`
  display: inline-block;
  background: white;
  padding: 0 12px;
  border-radius: 32px;
  font-size: 13px;
  margin-right: 0.2em;
`;
const PokemonCard: FunctionComponent<PokemonCardProps> = (props) => {
  const { pokemon } = props;
  const [showMoreInfo, setShowMoreInfo] = useState<boolean>(false);
  const Container = styled.div<PokemonCardProps>`
    padding: 0.5em;
    margin: 0.5em;
    border: none;
    border-radius: 10px;
    box-shadow: 0 8px 12px -6px black;
    -webkit-transition: all 0.5s ease-in-out;
    -moz-transition: all 0.5s ease-in-out;
    -ms-transition: all 0.5s ease-in-out;
    -o-transition: all 0.5s ease-in-out;
    opacity: 0.8;
    &:hover {
      box-shadow: 0 12px 18px -6px black;
      opacity: 1;
    }
    cursor: pointer;
    background: ${(props) => {
      if (props.pokemon.types.length > 1) {
        const test = props.pokemon.types.map((type) => {
          return `${PokemonTypeToColor(type.type.name)} 50%`;
        });
        return `linear-gradient(${test});`;
      } else {
        return PokemonTypeToColor(props.pokemon.types[0].type.name);
      }
    }};
  `;

  const HandlePokemonClick = () => {
    setShowMoreInfo(!showMoreInfo);
  };

  return (
    <div onClick={HandlePokemonClick}>
      <Container pokemon={pokemon}>
        <Number>#{pokemon.id}</Number>
        <Name>{pokemon.name}</Name>
        {showMoreInfo && (
          <>
            <Sprite
              id={pokemon.name}
              alt={`${pokemon.name}-sprite`}
              src={pokemon.sprites.front_default}
            ></Sprite>

            <Type>
              {pokemon.types.map((type) => {
                return <Chip>{type.type.name}</Chip>;
              })}
            </Type>
          </>
        )}
      </Container>
    </div>
  );
};

export default PokemonCard;
